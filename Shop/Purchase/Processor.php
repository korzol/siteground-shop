<?php
namespace Shop\Purchase;

use Shop\Purchase\Cart\Prepare;
use \Shop\Purchase\Cart\SpecialOffer\SpecialOffer;
use Shop\Purchase\Cart\UsualSale\Sale;

class Processor
{
    /**
     * @param array{int: int} $cart
     * @param array{
     *     int: array{
     *          id: int,
     *          name: string,
     *          price: int
     *   }
     * } $itemsInCart
     * @param array{
     *     int?: array{
     *          id: int,
     *          special_offer_id: int,
     *          good_id: int,
     *          quantity: int,
     *          price: int,
     *          created_at: string,
     *          updated_at: string
     *   }
     * } $specialOffer
     * @return array<'int',
     *      array{
     *          good_id: int,
     *          quantity: int,
     *          price: int,
     *          special_offer_id?: int
     *      }
     * >
     */
    public function run(array $cart, array $itemsInCart, array $specialOffer): array
    {
        $itemsUnderSpecialOffer = [];
        $itemsUsualSale = [];
        $specialOfferProcessor = new SpecialOffer();
        $usualSaleProcessor = new Sale();
        foreach (Prepare::run($cart) as $cartItem => $itemCount) {
            $itemsUnderSpecialOffer = array_merge(
                $itemsUnderSpecialOffer,
                $specialOfferProcessor->run((int)$cartItem, $itemCount, $specialOffer)
            );
            $itemsUsualSale = array_merge($itemsUsualSale, $usualSaleProcessor->run((int)$cartItem, $itemCount, $itemsInCart));
        }

        return array_merge($itemsUnderSpecialOffer, $itemsUsualSale);
    }
}
