<?php
namespace Shop\Purchase\Cart\SpecialOffer;

class SpecialOffer
{
    /**
     * @param int $itemId
     * @param int $itemCount
     * @param array{
     *     int: array{
     *          id: int,
     *          name: string,
     *          price: int
     *   }
     * } $itemsInCart
     * @param array{
     *     int?: array{
     *          id: int,
     *          special_offer_id: int,
     *          good_id: int,
     *          quantity: int,
     *          price: int,
     *          created_at: string,
     *          updated_at: string
     *   }
     * } $specialOffer
     * @return array{
     *     int?: array{
     *          good_id: int,
     *          quantity: int,
     *          price: int,
     *          special_offer_id: int
     *     }
     * }
     */
    public function run(int $itemId, int &$itemCount, array $specialOffer): array
    {
        if (! isset($specialOffer[$itemId])) {
            return [];
        }

        if ($this->isItemQualifiedForSpecialOffer($itemCount, $specialOffer[$itemId]['quantity'])) {
            return Calculator::run($itemId, $itemCount, $specialOffer[$itemId]);
        }

        return [];
    }

    /**
     * @param int $itemCount
     * @param int $quantity
     * @return bool
     */
    private function isItemQualifiedForSpecialOffer(int $itemCount, int $quantity): bool
    {
        return $itemCount >= $quantity;
    }
}
