<?php

namespace Shop\Purchase\Cart\SpecialOffer;

class Calculator
{
    /**
     * @param int $itemId
     * @param int $itemCount
     * @param array{id: int, name: string, price: int } $itemEntity
     * @param array{
     *     id: int,
     *     special_offer_id: int,
     *     good_id: int,
     *     quantity: int,
     *     price: int,
     *     created_at: string,
     *     updated_at: string
     * } $itemSpecialOffer
     * @return array{
     *     int?: array{
     *          good_id: int,
     *          quantity: int,
     *          price: int,
     *          special_offer_id: int
     *     }
     * }
     */
    public static function run(int $itemId, int &$itemCount, array $itemSpecialOffer): array
    {
        // TODO: refactor it
        $quantityInCartUnderSpecialOffer = (int)($itemCount / $itemSpecialOffer['quantity']);
        $itemCount -= $quantityInCartUnderSpecialOffer * $itemSpecialOffer['quantity'];

        $array = [];
        for ($i = 0; $i < $quantityInCartUnderSpecialOffer; ++$i) {
            $array[] = [
                'good_id' => $itemId,
                'quantity' => $itemSpecialOffer['quantity'],
                'price' => $itemSpecialOffer['price'],
                'special_offer_id' => $itemSpecialOffer['special_offer_id'],
            ];
        }

        return $array;
    }
}
