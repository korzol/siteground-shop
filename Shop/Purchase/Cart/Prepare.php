<?php
namespace Shop\Purchase\Cart;

class Prepare
{
    /**
     * @param array<int> $cart
     * @return array<int>
     */
    public static function run(array $cart): array
    {
        return array_count_values($cart);
    }
}
