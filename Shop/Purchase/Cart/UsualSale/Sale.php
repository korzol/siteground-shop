<?php

namespace Shop\Purchase\Cart\UsualSale;

class Sale
{
    /**
     * @param int $itemId
     * @param int $itemCount
     * @param array{
     *     int: array{
     *          id: int,
     *          name: string,
     *          price: int
     *   }
     * } $itemsInCart
     * @return array{
     *    int?: array{
     *      good_id: int,
     *      quantity: int,
     *      price: int
     *    }
     * }
     */
    public function run(int $itemId, int $itemCount, array $itemsInCart): array
    {
        if ($itemCount <= 0 || ! isset($itemsInCart[$itemId])) {
            return [];
        }

        return [Calculator::run($itemId, $itemCount, $itemsInCart[$itemId])];
    }
}
