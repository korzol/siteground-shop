<?php

namespace Shop\Purchase\Cart\UsualSale;

class Calculator
{
    /**
     * @param int $itemId
     * @param int $itemCount
     * @param array{id: int, name: string, price: int} $itemEntity
     * @return array{
     *    good_id: int,
     *    quantity: int,
     *    price: int
     * }
     */
    public static function run(int $itemId, int $itemCount, array $itemEntity): array
    {
        return [
            'good_id' => $itemId,
            'quantity' => $itemCount,
            'price' => $itemCount * $itemEntity['price'],
        ];
    }
}
