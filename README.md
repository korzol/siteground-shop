**This is a test project for siteground.**

There are still many things to improve, but I am very limited in time

***Installation:***

1. Clone the repository
2. cd ./siteground-shop

3. Issue the following cli commands:
    - ```make init```
    - ```make setup_db```
    - ```make seed_db```

It is possible that you would need to wait a few minutes for a mysql to set up after ```make init```  command.

Once initialization is done you can use the following cli commands to start and stop project: ```make up``` and ```make down```

There are following links available:

1. <a href="http://127.0.0.1:8086/list-goods" target="_blank">List available goods</a>

2. <a href="http://127.0.0.1:8086/cart" target="_blank">Cart</a>
3. <a href="http://127.0.0.1:8086/cart" target="_blank">Sales list</a>
4. <a href="http://127.0.0.1:8087/" target="_blank">PhpMyAdmin</a>

To remove project just issue:
```make destroy```
