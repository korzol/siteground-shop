<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $aId = \DB::table('goods')->insertGetId([
            'name' => 'A',
            'price' => 50,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $bId = \DB::table('goods')->insertGetId([
            'name' => 'B',
            'price' => 30,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $cId = \DB::table('goods')->insertGetId([
            'name' => 'C',
            'price' => 45,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $dId = \DB::table('goods')->insertGetId([
            'name' => 'D',
            'price' => 60,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $specialOfferId = \DB::table('special_offers')->insertGetId([
            'name' => 'Special Promotion',
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('special_offer_items')->insert([
            [
                'special_offer_id' => $specialOfferId,
                'good_id' => $aId,
                'quantity' => 3,
                'price' => 130,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'special_offer_id' => $specialOfferId,
                'good_id' => $bId,
                'quantity' => 2,
                'price' => 40,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'special_offer_id' => $specialOfferId,
                'good_id' => $cId,
                'quantity' => 3,
                'price' => 120,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'special_offer_id' => $specialOfferId,
                'good_id' => $dId,
                'quantity' => 4,
                'price' => 200,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
