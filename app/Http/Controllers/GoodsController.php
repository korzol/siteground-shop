<?php

namespace App\Http\Controllers;

use App\Models\Goods;

class GoodsController
{
    public function list()
    {
        $goods = Goods::with(
            'specialOfferItem',
            'specialOfferItem.specialOffer'
        )->get();

        return view('Goods.list', compact('goods'));
    }
}
