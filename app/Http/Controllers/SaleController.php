<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\Goods;
use App\Models\Sale;
use App\Models\SpecialOffer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Shop\Purchase\Processor;

class SaleController
{
    /**
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        $goods = Goods::all();
        return view('Cart.cart', compact('goods'));
    }

    public function buy(CartRequest $request)
    {
        $itemsIncart = Goods::query()->whereIn('id', $request->validated()['cart'])->get();

        $specialOffer = SpecialOffer::query()
            ->with('items')
            ->where('is_active', true)
            ->first();

        $purchase = new Processor();
        $specialOfferPrepared = [];
        if ($specialOffer) {
            $specialOfferPrepared = $specialOffer->items->keyBy('good_id')->toArray();
        }

        $results = $purchase->run(
            $request->validated()['cart'],
            $itemsIncart->keyBy('id')->toArray(),
            $specialOfferPrepared
        );

        if (count($results) > 0) {
            \DB::beginTransaction();
            try {
                $newSale = Sale::query()->create([
                    'total_price' => array_sum(array_column($results, 'price')),
                ]);

                $newSale->items()->createMany($results);

                \DB::commit();

                return redirect()->route('sales-list');
            } catch(\Throwable $e) {
                \DB::rollBack();

                return redirect()->route('cart-calculate');
            }
        }
    }

    public function list()
    {
        $sales = Sale::with(
            'items',
            'items.good'
        )
            ->orderByDesc('id')
            ->get();

        return view('Sales.list', compact('sales'));
    }
}
