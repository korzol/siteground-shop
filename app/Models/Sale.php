<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Sale extends Model
{
    protected $table = 'sales';

    protected $fillable = [
        'total_price',
    ];

    public function items(): HasMany
    {
        return $this->hasMany(SaleItems::class);
    }
}
