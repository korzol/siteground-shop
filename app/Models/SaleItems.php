<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SaleItems extends Model
{
    protected $table = 'sale_items';

    protected $fillable = [
        'sale_id',
        'good_id',
        'quantity',
        'price',
        'special_offer_id',
    ];

    public function good():belongsTo
    {
        return $this->belongsTo(Goods::class);
    }
}
