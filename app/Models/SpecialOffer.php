<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SpecialOffer extends Model
{
    protected $table = 'special_offers';

    protected $fillable = [
        'name',
        'is_active',
    ];

    protected $casts = [
        'is_active' => 'bool',
    ];

    public function items(): HasMany
    {
        return $this->hasMany(SpecialOfferItem::class);
    }
}
