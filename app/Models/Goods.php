<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Goods extends Model
{
    protected $table = 'goods';

    protected $fillable = [
        'name',
        'price',
        'special_offer_qty',
        'special_offer_price',
    ];

    public function specialOfferItem(): HasOne
    {
        return $this->hasOne(SpecialOfferItem::class, 'good_id', 'id');
    }
}
