<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SpecialOfferItem extends Model
{
    protected $table = 'special_offer_items';

    protected $fillable = [
        'special_offer_id',
        'good_id',
        'quantity',
        'price',
    ];

    public function specialOffer(): BelongsTo
    {
        return $this->belongsTo(SpecialOffer::class);
    }
}
