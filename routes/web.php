<?php

use App\Http\Controllers\GoodsController;
use App\Http\Controllers\SaleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/list-goods', [GoodsController::class, 'list']);

Route::get('/cart', [SaleController::class, 'index']);
Route::post('/buy', [SaleController::class, 'buy'])->name('cart-calculate');
Route::get('/list', [SaleController::class, 'list'])->name('sales-list');
