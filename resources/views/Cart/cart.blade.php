@extends('Layouts.main')

@section('content')
    <div class="container row">
        <div class="col-6">
            <div class="form-group">
                <label>Select item:</label>
                <select
                    class="form-control"
                    id="cart-item-selector"
                >
                    <option disabled selected>Add Item Into Cart</option>
                    @foreach($goods as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <button
                type="button"
                class="btn btn-info"
                id="add-to-cart"
            >
                Add To Cart
            </button>
        </div>
        <div class="col-6">
            Cart Content
            <div class="col-12">
                <form id="cart-form" action="{{ route('cart-calculate') }}" method="POST">
                    @csrf
                    <table
                        class="table table-striped"
                        id="cart-content"
                    >
                        <thead>
                            <tr>
                                <th>Item</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <button
                        type="button"
                        class="btn btn-primary"
                        id="calculate-cart"
                    >
                        Calculate
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#add-to-cart').on('click', function(){
            let selectedItem = $('#cart-item-selector option:selected');
            let selectedItemId = selectedItem.val();
            if(! isNaN(parseInt(selectedItemId))) {
                let selectedItemName = selectedItem.text();
                let tableRow = '<tr><td><input name="cart[]" type="hidden" value="'+selectedItemId+'"> '+selectedItemName+'</td></tr>';

                $('#cart-content > tbody:last-child').append(tableRow);
            }
        });

        $('#calculate-cart').on('click', function(){
            $('#cart-form').submit();
        });
    </script>
@endpush
