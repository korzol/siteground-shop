@extends('Layouts.main')

@section('content')
    @foreach($sales as $sale)
        <h4>Sale ID: {{$sale->id}}, Total: {{ $sale->total_price }}</h4>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Special Offer Price</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sale->items as $item)
                    <tr>
                        <td>{{ $item->good->name }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->special_offer_id ? '' :$item->price }}</td>
                        <td>{{ $item->special_offer_id ? $item->price : '' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
@endsection
