@extends('Layouts.main')

@section('content')
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Special Price</th>
        </tr>
    </thead>
    <tbody>
    @foreach($goods as $item)
        <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->specialOfferItem->quantity }}</td>
            <td>{{ $item->specialOfferItem->price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection
