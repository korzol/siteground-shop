#!/usr/bin/env php
<?php

$priceList = [
        'A' => 50,
        'B' => 30,
        'C' => 25,
        'D' => 75
];

$specialOffer = [
        'A' => [
                'quantity' => 2,
                'price' => 75
        ],
        'B' => [
                'quantity' => 3,
                'price' => 60
        ],
];

// AABBC
$basket = [
        'A',
        'A',
        'B',
        'C',
        'B',
        'A',
        'A',
];

function calculateUsualPrice(int $quantity, int $price): int
{
    return $quantity * $price;
}

// CartItems is better name
$basketEntriesAndPrices = [];
$tmpArray = [];
foreach($basket as $item) {
    $tmpArray[$item][] = $item;
    if (
            isset($specialOffer[$item]) &&
            isset($specialOffer[$item]['quantity']) &&
            $specialOffer[$item]['quantity'] &&
            $specialOffer[$item]['price']
    ) {
        if (count($tmpArray) === $specialOffer[$item]['quantity']) {
            $basketEntriesAndPrices[] = [
                    'item' => $item,
                    'specialOfferPrice' => $specialOffer[$item]['price']
            ];

            unset($tmpArray[$item]);
        }
    } // We are done
    // We gonna need another foreach through tmpArray in order to get items which are not under special offer
}

print_r($tmpArray); exit();

$basketSorted = array_count_values($basket);
//print_r($basketSorted); exit();
$basketPrices = [];
foreach ($basketSorted as $basketItem => $basketItemQty) {
    if (isset($specialOffer[$basketItem])) {
        if ($basketItemQty >= $specialOffer[$basketItem]['quantity']) {
            $quantityInBasketUnderSpecialOffer = (int)($basketItemQty / $specialOffer[$basketItem]['quantity']);
            $qtyItemsNotCoveredByOffer = $basketItemQty % $specialOffer[$basketItem]['quantity'];
            // It is better to have several entries for special entries offer if for example there is 2xA and 2xA
            $basketPrices[$basketItem] = [
                    'specialOffer' => $quantityInBasketUnderSpecialOffer * $specialOffer[$basketItem]['price'],
                    'usualPrice' => calculateUsualPrice($qtyItemsNotCoveredByOffer, $priceList[$basketItem]),
            ];
        } else {
            $basketPrices[$basketItem] = [
                'usualPrice' => calculateUsualPrice($basketItemQty, $priceList[$basketItem]),
            ];
        }
    } else {
        $basketPrices[$basketItem] = [
                'usualPrice' => calculateUsualPrice($basketItemQty, $priceList[$basketItem]),
        ];
    }
}


print_r($basketPrices);