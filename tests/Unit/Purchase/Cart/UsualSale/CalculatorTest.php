<?php
namespace tests\Unit\Purchase\Cart\UsualSale;

use PHPUnit\Framework\TestCase;
use \Shop\Purchase\Cart\UsualSale\Calculator;

class CalculatorTest extends TestCase
{
    public function testRun()
    {
        $itemId = 1;
        $itemCount = 3;

        $good = [
            'id' => 1,
            'name' => 'A',
            'price' => 40,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $expectedArray = [
            'good_id' => 1,
            'quantity' => 3,
            'price' => 120,
        ];

        $calculator = new Calculator();

        $this->assertEquals($expectedArray, $calculator->run($itemId, $itemCount, $good));
    }

    public function testRunZeroQuantity(): void
    {
        $itemId = 1;
        $itemCount = 0;

        $good = [
            'id' => 1,
            'name' => 'A',
            'price' => 40,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $expectedArray = [
            'good_id' => 1,
            'quantity' => 0,
            'price' => 0,
        ];

        $sale = new Calculator();

        $this->assertEquals($expectedArray, $sale->run($itemId, $itemCount, $good));
    }
}
