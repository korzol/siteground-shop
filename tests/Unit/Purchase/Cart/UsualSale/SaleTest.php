<?php
namespace tests\Unit\Purchase\Cart\UsualSale;

use PHPUnit\Framework\TestCase;
use Shop\Purchase\Cart\UsualSale\Sale;

class SaleTest extends TestCase
{
    public function testRun(): void
    {
        $itemId = 1;
        $itemCount = 3;

        $goods = [
            1 => [
                'id' => 1,
                'name' => 'A',
                'price' => 40,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        $expectedArray = [
            [
                'good_id' => 1,
                'quantity' => 3,
                'price' => 120,
            ]
        ];

        $sale = new Sale();

        $this->assertEquals($expectedArray, $sale->run($itemId, $itemCount, $goods));
    }

    public function testRunZeroQuantity(): void
    {
        $itemId = 1;
        $itemCount = 0;

        $goods = [
            1 => [
                'id' => 1,
                'name' => 'A',
                'price' => 40,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        $expectedArray = [];

        $sale = new Sale();

        $this->assertEquals($expectedArray, $sale->run($itemId, $itemCount, $goods));
    }

    public function testRunNoGoods(): void
    {
        $itemId = 1;
        $itemCount = 4;

        $goods = [];

        $expectedArray = [];

        $sale = new Sale();

        $this->assertEquals($expectedArray, $sale->run($itemId, $itemCount, $goods));
    }
}
