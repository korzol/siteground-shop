<?php
namespace tests\Unit\Purchase\Cart\SpecialOffer;

use PHPUnit\Framework\TestCase;
use Shop\Purchase\Cart\SpecialOffer\Calculator;

class CalculatorTest extends TestCase
{
    public function testCalculator()
    {
        $itemId = 1;
        $itemCount = 3;

        $offer = [
            'id' => 1,
            'special_offer_id' => 1,
            'good_id' => 1,
            'quantity' => 3,
            'price' => 80,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $expectedArray = [
            [
                'good_id' => 1,
                'quantity' => 3,
                'price' => 80,
                'special_offer_id' => 1,
            ]
        ];

        $calculator = new Calculator();

        $this->assertEquals($expectedArray, $calculator->run($itemId, $itemCount, $offer));
    }

    public function testCalculatorInsufficientQuantity()
    {
        $itemId = 1;
        $itemCount = 3;

        $offer = [
            'id' => 1,
            'special_offer_id' => 1,
            'good_id' => 1,
            'quantity' => 30,
            'price' => 80,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $expectedArray = [];

        $calculator = new Calculator();

        $this->assertEquals($expectedArray, $calculator->run($itemId, $itemCount, $offer));
    }
}
