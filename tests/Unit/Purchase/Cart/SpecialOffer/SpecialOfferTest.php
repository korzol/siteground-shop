<?php
namespace tests\Unit\Purchase\Cart\SpecialOffer;

use PHPUnit\Framework\TestCase;
use Shop\Purchase\Cart\SpecialOffer\SpecialOffer;

class SpecialOfferTest extends TestCase
{
    public function testRun(): void
    {
        $itemId = 1;
        $itemCount = 3;

        $offer = [
            1 => [
                'id' => 1,
                'special_offer_id' => 1,
                'good_id' => 1,
                'quantity' => 3,
                'price' => 80,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        $expectedArray = [
            [
                'good_id' => 1,
                'quantity' => 3,
                'price' => 80,
                'special_offer_id' => 1,
            ]
        ];

        $specialOffer = new SpecialOffer();

        $this->assertEquals($expectedArray, $specialOffer->run($itemId, $itemCount, $offer));
    }

    public function testRunNoResult(): void
    {
        $itemId = 1;
        $itemCount = 3;

        $offer = [];

        $expectedArray = [];

        $specialOffer = new SpecialOffer();

        $this->assertEquals($expectedArray, $specialOffer->run($itemId, $itemCount, $offer));
    }
}
