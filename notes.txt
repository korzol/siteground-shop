
Goods:
id | name | price | special_offer_qty | special_offer_price
// Просто распишешь им что это тестовая задача. Но в реальных условиях special_offer должно быть в отдельной таблице и иметь дату начала и дату конца. А так как мы не делаем реальный магазин, то и нет смысла в таком усложнении

Special_offers:
id | name | good_id | quantity | price | is_active // It should be so under real conditions




Sales:
id | total_price | timestamps

Sale_items:
id | sale_id | good_id | quantity | price | is_special_offer | special_offer_id
